from django.shortcuts import render, HttpResponse

# Create your views here.

def home(request):
	numbers = [1,2,3]
	name ='John Doe'
	args={'myName': name, 'numbers': numbers}
	return render(request,'newapp/login.html',args)